package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton

val table_onboard="Onboard"
val table_skip="Skip"
val key_is_skip="isSkip"
val key_id="id"

class OnboardingFragment : Fragment() {

    var preff:SharedPreferences?=null
    var preff_skip:SharedPreferences?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val l= inflater.inflate(R.layout.fragment_onboarding, container, false)
        preff=activity?.getSharedPreferences(table_onboard, Context.MODE_PRIVATE)
        preff_skip=activity?.getSharedPreferences(table_skip, Context.MODE_PRIVATE)
        preff?.edit()?.putInt(key_id, 0)?.apply()
        val btn_skip:AppCompatButton=l.findViewById(R.id.skip1)
        btn_skip.setOnClickListener {
            val intent= Intent(activity, HolderActivity::class.java)
            startActivity(intent)
            preff_skip?.edit()?.putBoolean(key_is_skip, true)?.apply()
        }
        return l
    }



}