package com.example.myapplication

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentTransaction

class OnboardingActivity : AppCompatActivity() {
    var preff_onboard:SharedPreferences?=null
    var fraglist= listOf(OnboardingFragment(), Onboarding2Fragment(), Onboarding3Fragment())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)
        preff_onboard=getSharedPreferences(table_onboard, MODE_PRIVATE)
        val id: Int? =preff_onboard?.getInt(key_id, 0)
        supportFragmentManager.beginTransaction().replace(R.id.holder, fraglist[id!!.toInt()]).commit()
    }
    fun next1(view: View) {
        supportFragmentManager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.holder, fraglist[1]).commit()

    }

    fun next2(view: View) {
        supportFragmentManager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.holder, fraglist[2]).commit()
    }

}