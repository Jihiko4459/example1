package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton


class Onboarding3Fragment : Fragment() {

    var preff_skip: SharedPreferences?=null
    var preff: SharedPreferences?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val l= inflater.inflate(R.layout.fragment_onboarding3, container, false)
        preff_skip=activity?.getSharedPreferences(table_skip, Context.MODE_PRIVATE)
        preff=activity?.getSharedPreferences(table_onboard, Context.MODE_PRIVATE)
        preff?.edit()?.putInt(key_id, 2)?.apply()
        val btn_sign_up:AppCompatButton=l.findViewById(R.id.sign_up)
        val btn_sign_in:AppCompatButton=l.findViewById(R.id.sign_in)
        btn_sign_up.setOnClickListener {
            preff?.edit()?.putBoolean(key_is_skip, true)?.apply()
            val intent= Intent(activity, HolderActivity::class.java)
            startActivity(intent)
        }
        btn_sign_in.setOnClickListener {
            preff_skip?.edit()?.putBoolean(key_is_skip, true)?.apply()
            val intent= Intent(activity, HolderActivity::class.java)
            startActivity(intent)
        }
        return l
    }

}