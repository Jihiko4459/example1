package com.example.myapplication

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.Window

class MainActivity : AppCompatActivity() {
    var preff_skip:SharedPreferences?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        preff_skip=getSharedPreferences(table_skip, MODE_PRIVATE)
        val isSkip:Boolean=preff_skip?.getBoolean(key_is_skip, false)!!

        val timer=object:CountDownTimer(3000, 1000){
            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                if (isSkip){
                    val intent= Intent(this@MainActivity, HolderActivity::class.java)
                    startActivity(intent)
                }
                else{
                    val intent= Intent(this@MainActivity, OnboardingActivity::class.java)
                    startActivity(intent)
                }

            }
        }
        timer.start()
    }
}